﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flexlive.CQP.CSharpPlugins.Demo
{
    public class TulingHelper
    {
        public static UserInfoModel userInfoModel = new UserInfoModel()
        {
            apiKey = "9ee02493d67c466689be4b417a87e9e8",
            userId = "272898"
        };
        public static SelfInfoModel selfInfoModel = new SelfInfoModel
        {
            location = new LocationModel
            {
                city = "广州",
                province = "广东",
                street = "天河区"
            }
        };
        public static string RequestTulingAndGetResponseByMsg(string msg, long fromQQ)
        {
            var input = new TulingRequestModel()
            {
                reqType = 0,
                userInfo = new UserInfoModel
                {
                    apiKey = userInfoModel.apiKey,
                    userId = fromQQ.ToString()
                },
                perception = new PerceptionModel
                {
                    selfInfo = null,
                    inputText = new InputTextModel
                    {
                        text = msg
                    }
                }
            };
            var responseModel = WebRequestUtil.PostAndResponse<TulingResponseModel, TulingRequestModel>("http://openapi.tuling123.com/openapi/api/v2", input, null);
            if (responseModel == null || responseModel.intent == null || responseModel.results == null || responseModel.intent.code == 5000) return string.Empty;

            StringBuilder sb = new StringBuilder();
            foreach (var result in responseModel.results.Where(o => o.resultType == "text"))
            {
                sb.Append($"{result.values.text}\r\n");
            }
            foreach (var result in responseModel.results.Where(o => o.resultType == "url"))
            {
                sb.Append($"{result.values.url}\r\n");
            }
            return sb.ToString();
        }
    }

    public class TulingRequestModel
    {
        public TulingRequestModel()
        {
            reqType = 0;
        }
        /// <summary>
        /// 输入类型:0-文本(默认)、1-图片、2-音频
        /// </summary>
        public int reqType { get; set; }

        public PerceptionModel perception { get; set; }

        public UserInfoModel userInfo { get; set; }


    }
    public class PerceptionModel
    {
        public InputTextModel inputText { get; set; }
        public InputImageModel inputImage { get; set; }
        public SelfInfoModel selfInfo { get; set; }
    }
    public class InputTextModel
    {
        public string text { get; set; }
    }
    public class InputImageModel
    {
        public string url { get; set; }
    }
    public class SelfInfoModel
    {
        public LocationModel location { get; set; }
    }

    public class LocationModel
    {
        /// <summary>
        /// 市：北京
        /// </summary>
        public string city { get; set; }

        /// <summary>
        /// 省：北京
        /// </summary>
        public string province { get; set; }

        /// <summary>
        /// 街道/道路：信息路
        /// </summary>
        public string street { get; set; }
    }

    public class UserInfoModel
    {
        /// <summary>
        /// 机器人标识
        /// </summary>
        public string apiKey { get; set; }

        /// <summary>
        /// 用户唯一标识
        /// </summary>
        public string userId { get; set; }
    }

    public class TulingResponseModel
    {
        public IntentModel intent { get; set; }

        public List<ResultModel> results { get; set; }

    }
    public class IntentModel
    {
        /// <summary>
        /// 输出功能code,5000为无解析结果
        /// </summary>
        public int code { get; set; }

        public string intentName { get; set; }
        public string actionName { get; set; }
        public object parameters { get; set; }
    }

    public class ResultModel
    {
        /// <summary>
        /// ‘组’编号:0为独立输出，大于0时可能包含同组相关内容 (如：音频与文本为一组时说明内容一致)
        /// </summary>
        public int groupType { get; set; }

        /// <summary>
        /// 文本(text);连接(url);音频(voice);视频(video);图片(image);图文(news)
        /// </summary>
        public string resultType { get; set; }

        public ResultValueModel values { get; set; }
    }

    public class ResultValueModel
    {
        public string text { get; set; }
        public string url { get; set; }

    }
}
