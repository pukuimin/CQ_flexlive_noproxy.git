﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Flexlive.CQP.CSharpPlugins.Demo
{
    public static class WebRequestUtil
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static T GetAndResponse<T>(string url, IDictionary<string, string> headers = null, string contentType = "application/json; charset=utf-8")
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                request.Method = "GET";
                request.Headers.Add("Accept-Encoding", "gzip");
                request.AllowAutoRedirect = true;
                request.Proxy = null;
                request.ContentType = contentType;
                if (headers != null)
                {
                    //request.SetRequestHeader(headers);
                }
                //// 记录请求开始后到获得响应所耗时间(不包括数据传输)并重置计时器
                //var counter = new Stopwatch();
                //counter.Start();

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    //counter.Stop();
                    //var requestCost = counter.ElapsedMilliseconds;
                    //counter.Reset();
                    T result = default(T);
                    // 记录数据传输与本地处理数据所耗时间
                    //counter.Start();
                    string json = response.GetResponseText("utf-8");
                    //counter.Stop();
                    //var processCost = counter.ElapsedMilliseconds;
                    //if (requestCost + processCost >= 1000)
                    //{
                    //    LogRequestCostTime(url, requestCost, processCost);
                    //}
                    //if (json.Contains(NeedLoginNoticeText)) throw new BusinessException(NeedLoginNoticeText);
                    //if (json.Contains(ServerInnerError)) throw new BusinessException(ServerInnerError);
                    if (typeof(T) == typeof(string)) result = (T)Convert.ChangeType(json, typeof(T), CultureInfo.CurrentCulture);
                    else result = JsonUtility.FromJsonTo<T>(json);
                    return result;
                }
            }
            finally
            {
                TryCatch(() =>
                {
                    request.Abort();
                });
            }
        }

        //public static string NeedLoginNoticeText = "该操作需要登录后才能进行";
        //public static string ServerInnerError = "服务器内部出错";

        /// <summary>
        /// Post
        /// </summary>
        /// <typeparam name="TP">自定义类型</typeparam>
        /// <param name="url">url</param>
        /// <param name="method">请求方式，暂时只支持Post。Get请调用其他</param>
        /// <param name="data">数据，类型为UserType，会自动序列化作为json将数据Post出去</param>
        /// <param name="contentType">RequestHeader:contentType</param>
        /// <returns>返回反序列化后的实例</returns>
        public static TR PostAndResponse<TR, TP>(string url, TP data, IDictionary<string, string> headers = null, string contentType = "application/json; charset=utf-8", int timeout = -1, string formDataString = null)
        {
            TR result = default(TR);
            string content = string.Empty;
            if (contentType.ToLower().Contains("json"))
            {
                content = JsonUtility.ToJson(data);
            }
            else if (contentType.ToLower().Contains("x-www-form-urlencoded"))
            {
                content = formDataString;
            }
            //else
            //{
            //    //未作处理，返回默认
            //    return default(TR);
            //}
            //content = "certificate=440921199109173583&phone=13822290409&service_item_code=7007&service_item_name=%E6%9C%AC%E7%A7%91%EF%BC%88%E5%90%AB%E4%B8%AD%E7%BA%A7%E8%81%8C%E7%A7%B0%EF%BC%89%E4%BA%BA%E6%89%8D%E5%BC%95%E8%BF%9B&address=%E5%A4%A9%E6%B2%B3%E5%8C%97%E8%B7%AF894%E5%8F%B7%EF%BC%88%E5%92%A8%E8%AF%A2%E7%94%B5%E8%AF%9D%EF%BC%9A020-38470847%EF%BC%89&addressMsg=3&org_code=G34014976&time=08%3A30%3A00--09%3A30%3A00&predate=2018-05-24";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                if (timeout > 0)
                {
                    request.Timeout = timeout;
                    request.ReadWriteTimeout = timeout;
                }
                request.Method = "POST";
                request.Headers.Add("Accept-Encoding", "gzip");
                request.ContentType = contentType;
                request.AllowAutoRedirect = true;
                request.Proxy = null;
                if (headers != null)
                {
                    //request.SetRequestHeader(headers);
                }
                byte[] byteData;
                if (contentType.ToLower().Contains("charset=utf-8"))
                {
                    byteData = Encoding.GetEncoding("UTF-8").GetBytes(content);
                    //request.ContentType += "; charset=utf-8";
                }
                else
                {
                    //未作处理，返回默认
                    //request = null;
                    //return default(TR);
                    byteData = Encoding.Default.GetBytes(content);
                }


                using (Stream stream = request.GetRequestStream())
                {
                    stream.Write(byteData, 0, (int)byteData.Length);
                    stream.Close();
                    //// 记录请求开始后到获得响应所耗时间(不包括数据传输)并重置计时器
                    //var counter = new Stopwatch();
                    //counter.Start();
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        //counter.Stop();
                        //var requestCost = counter.ElapsedMilliseconds;
                        //counter.Reset();
                        //// 记录数据传输与本地处理数据所耗时间
                        //counter.Start();
                        string json = response.GetResponseText("utf-8");
                        //counter.Stop();
                        //var processCost = counter.ElapsedMilliseconds;
                        //if (requestCost + processCost >= 1000)
                        //{
                        //    LogRequestCostTime(url, requestCost, processCost);
                        //}
                        //if (json.Contains(NeedLoginNoticeText)) throw new BusinessException(NeedLoginNoticeText);
                        //if (json.Contains(ServerInnerError)) throw new BusinessException(ServerInnerError);
                        if (typeof(TR) == typeof(string)) result = (TR)Convert.ChangeType(json, typeof(TR), CultureInfo.CurrentCulture);
                        else result = JsonUtility.FromJsonTo<TR>(json);
                        return result;
                    }
                }
            }
            finally
            {
                TryCatch(() =>
                {
                    request.Abort();
                });
            }

        }

        public static string FromDictToFormDataString(Dictionary<string, string> dict)
        {
            if (dict == null || dict.Count == 0) return string.Empty;
            var list = new List<string>();
            foreach (var d in dict)
            {
                var encode = d.Value;// System.Web.HttpUtility.UrlEncode(d.Value, Encoding.UTF8);
                //if (encode != null && encode.Contains("%")) encode = encode.ToUpperInvariant();
                list.Add(string.Format("{0}={1}", d.Key, encode));
            }
            var str = string.Join("&", list);
            list.Clear();
            return str;
        }
        private static void TryCatch(Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                //Log4netHelper.Error("TryCatch", ex);
            }
        }

    }
}
