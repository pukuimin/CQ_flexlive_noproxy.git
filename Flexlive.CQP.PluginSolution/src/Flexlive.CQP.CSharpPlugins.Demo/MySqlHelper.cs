﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Flexlive.CQP.CSharpPlugins.Demo
{
    /// <summary>
    /// 非静态方法版本的Mysql帮助类
    /// </summary>
    public class MySqlHelper
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MySqlHelper"/> class.
        /// </summary>
        /// <param name="str">数据库连接串或ConnectionStrings的Key.</param>
        /// <param name="type">类型：默认为1-数据库连接串，2-ConnectionStrings配置Key.</param>
        public MySqlHelper(string str, int type = 1)
        {
            ////Server=192.168.9.224;Port=3306;Database=ycf_spider_product;Uid=root;Pwd=ycf_test;Charset=utf8;Convert Zero Datetime=True
            //ConfigurationManager.ConnectionStrings["BigDataMySqlConnection"].ConnectionString
            if (type == 1) mySqlConnectionString = str;
            else if (type == 2) mySqlConnectionString = ConfigurationManager.ConnectionStrings[str].ConnectionString;
        }
        private string mySqlConnectionString { get; set; }

        /// <summary> 
        /// 执行SQL语句，返回影响的记录数 
        /// </summary> 
        /// <param name="sqlString">SQL语句</param> 
        /// <returns>影响的记录数</returns> 
        public int ExecuteNonQuery(string sqlString, params MySqlParameter[] cmdParms)
        {

            using (MySqlConnection _mySqlCon = new MySqlConnection(mySqlConnectionString))
            {
                using (MySqlCommand cmd = new MySqlCommand(sqlString, _mySqlCon))
                {
                    try
                    {
                        PrepareCommand(cmd, _mySqlCon, null, sqlString, cmdParms);
                        int rows = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        return rows;
                    }
                    catch (MySql.Data.MySqlClient.MySqlException ex)
                    {
                        throw ex;
                    }
                }
            }
        }

        private void PrepareCommand(MySqlCommand cmd, MySqlConnection conn, MySqlTransaction trans, string cmdText, MySqlParameter[] cmdParms)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            cmd.Connection = conn;
            cmd.CommandText = cmdText;
            if (trans != null)
                cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;//cmdType; 
            if (cmdParms != null)
            {
                foreach (MySqlParameter parameter in cmdParms)
                {
                    if ((parameter.Direction == ParameterDirection.InputOutput || parameter.Direction == ParameterDirection.Input) &&
                    (parameter.Value == null))
                    {
                        parameter.Value = DBNull.Value;
                    }
                    cmd.Parameters.Add(parameter);
                }
            }
        }

        //执行SQL语句，返回影响的记录数 
        /// <summary> 
        /// 执行SQL语句，返回影响的记录数 
        /// </summary> 
        /// <param name="sqlString">SQL语句</param> 
        /// <returns>影响的记录数</returns> 
        public int ExecuteNonQuery(string sqlString)
        {
            using (MySqlConnection _mySqlCon = new MySqlConnection(mySqlConnectionString))
            {
                using (MySqlCommand cmd = new MySqlCommand(sqlString, _mySqlCon))
                {
                    try
                    {
                        _mySqlCon.Open();
                        int rows = cmd.ExecuteNonQuery();
                        return rows;
                    }
                    catch (MySql.Data.MySqlClient.MySqlException ex)
                    {
                        _mySqlCon.Close();
                        throw ex;
                    }
                }
            }
        }

        //执行多条SQL语句，实现数据库事务。 
        /// <summary> 
        /// 执行多条SQL语句，实现数据库事务。 
        /// </summary> 
        /// <param name="sqlStringList">多条SQL语句</param> 
        public bool ExecuteNoQueryTran(List<String> sqlStringList)
        {
            using (MySqlConnection conn = new MySqlConnection(mySqlConnectionString))
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = conn;
                MySqlTransaction tx = conn.BeginTransaction();
                cmd.Transaction = tx;
                try
                {
                    for (int n = 0; n < sqlStringList.Count; n++)
                    {
                        string strsql = sqlStringList[n];
                        if (strsql.Trim().Length > 1)
                        {
                            cmd.CommandText = strsql;
                            PrepareCommand(cmd, conn, tx, strsql, null);
                            cmd.ExecuteNonQuery();
                        }
                    }
                    //cmd.ExecuteNonQuery();
                    tx.Commit();
                    return true;
                }
                catch (MySqlException ex)
                {
                    tx.Rollback();
                    throw ex;
                }
            }
        }


        /// <summary> 
        /// 执行一条计算查询结果语句，返回查询结果（object）。 
        /// </summary> 
        /// <param name="sqlString">计算查询结果语句</param> 
        /// <returns>查询结果（object）</returns> 
        public object ExecuteScalar(string sqlString)
        {
            using (MySqlConnection connection = new MySqlConnection(mySqlConnectionString))
            {
                using (MySqlCommand cmd = new MySqlCommand(sqlString, connection))
                {
                    try
                    {
                        connection.Open();
                        object obj = cmd.ExecuteScalar();
                        if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
                        {
                            return null;
                        }
                        else
                        {
                            return obj;
                        }
                    }
                    catch (MySqlException ex)
                    {
                        connection.Close();
                        throw ex;
                    }
                }
            }
        }
        /// <summary> 
        /// 执行一条计算查询结果语句，返回查询结果（object）。 
        /// </summary> 
        /// <param name="sqlString">计算查询结果语句</param> 
        /// <returns>查询结果（object）</returns> 
        public object ExecuteScalar(string sqlString, params MySqlParameter[] cmdParms)
        {
            using (MySqlConnection connection = new MySqlConnection(mySqlConnectionString))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        PrepareCommand(cmd, connection, null, sqlString, cmdParms);
                        object obj = cmd.ExecuteScalar();
                        cmd.Parameters.Clear();
                        if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
                        {
                            return null;
                        }
                        else
                        {
                            return obj;
                        }
                    }
                    catch (MySql.Data.MySqlClient.MySqlException e)
                    {
                        throw e;
                    }
                }
            }
        }

        /// <summary> 
        /// 执行查询语句，返回MySqlDataReader ( 注意：调用该方法后，一定要对MySqlDataReader进行Close ) 
        /// </summary> 
        /// <param name="strSQL">查询语句</param> 
        /// <returns>MySqlDataReader</returns> 
        public MySqlDataReader ExecuteReader(string strSQL)
        {
            MySqlConnection connection = new MySqlConnection(mySqlConnectionString);
            MySqlCommand cmd = new MySqlCommand(strSQL, connection);
            try
            {
                connection.Open();
                MySqlDataReader myReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return myReader;
            }
            catch (MySql.Data.MySqlClient.MySqlException e)
            {
                throw e;
            }
        }
        /// <summary> 
        /// 执行查询语句，返回MySqlDataReader ( 注意：调用该方法后，一定要对MySqlDataReader进行Close ) 
        /// </summary> 
        /// <param name="strSQL">查询语句</param> 
        /// <returns>MySqlDataReader</returns> 
        public MySqlDataReader ExecuteReader(string sqlString, params MySqlParameter[] cmdParms)
        {
            MySqlConnection connection = new MySqlConnection(mySqlConnectionString);
            MySqlCommand cmd = new MySqlCommand();
            try
            {
                PrepareCommand(cmd, connection, null, sqlString, cmdParms);
                MySqlDataReader myReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                cmd.Parameters.Clear();
                return myReader;
            }
            catch (MySql.Data.MySqlClient.MySqlException e)
            {
                throw e;
            }
        }

        /// <summary> 
        /// 执行查询语句，返回DataTable 
        /// </summary> 
        /// <param name="sqlString">查询语句</param> 
        /// <returns>DataTable</returns> 
        public DataTable ExecuteDataTable(string sqlString)
        {
            using (MySqlConnection connection = new MySqlConnection(mySqlConnectionString))
            {
                DataSet ds = new DataSet();
                try
                {
                    connection.Open();
                    MySqlDataAdapter command = new MySqlDataAdapter(sqlString, connection);
                    command.Fill(ds, "ds");
                }
                catch (MySql.Data.MySqlClient.MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }
                return ds.Tables[0];
            }
        }
        /// <summary> 
        /// 执行查询语句，返回DataTable 
        /// </summary> 
        /// <param name="sqlString">查询语句</param> 
        /// <returns>DataTable</returns> 
        public List<T> ExecuteModels<T>(string sqlString)
        {
            List<T> list = new List<T>();
            DataSet ds = new DataSet();
            using (MySqlConnection connection = new MySqlConnection(mySqlConnectionString))
            {

                try
                {
                    connection.Open();
                    MySqlDataAdapter command = new MySqlDataAdapter(sqlString, connection);
                    command.Fill(ds, "ds");
                }
                catch (MySql.Data.MySqlClient.MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            Type type = typeof(T);
            var properties = type.GetProperties();
            try
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    T model = Activator.CreateInstance<T>();
                    foreach (DataColumn column in row.Table.Columns)
                    {
                        PropertyInfo p = properties.FirstOrDefault(o => string.Equals(o.Name, column.ColumnName, StringComparison.InvariantCultureIgnoreCase));
                        if (p != null)
                        {
                            var value = row[column.ColumnName];
                            if (value == DBNull.Value)
                            {
                                value = null;
                            }
                            p.SetValue(model, value, null);
                        }
                    }
                    list.Add(model);
                }
            }
            catch (Exception ex)
            {
                //LogManager.GetLogger(this.GetType()).ErrorFormat("mysql读取出错,sql:{0}，ex:{1}", sqlString, ex);
                throw new BusinessException("ExecuteModels转换出错。", ex);
            }
            finally
            {
                if (ds != null) ds.Clear();
            }
            return list;
        }
        /// <summary> 
        /// 执行查询语句，返回DataSet 
        /// </summary> 
        /// <param name="sqlString">查询语句</param> 
        /// <returns>DataTable</returns> 
        public DataTable ExecuteDataTable(string sqlString, params MySqlParameter[] cmdParms)
        {
            using (MySqlConnection connection = new MySqlConnection(mySqlConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand();
                PrepareCommand(cmd, connection, null, sqlString, cmdParms);
                using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    try
                    {
                        da.Fill(ds, "ds");
                        cmd.Parameters.Clear();
                    }
                    catch (MySql.Data.MySqlClient.MySqlException ex)
                    {
                        throw new Exception(ex.Message);
                    }
                    return ds.Tables[0];
                }
            }
        }
        //获取起始页码和结束页码 
        public DataTable ExecuteDataTable(string cmdText, int startResord, int maxRecord)
        {
            using (MySqlConnection connection = new MySqlConnection(mySqlConnectionString))
            {
                DataSet ds = new DataSet();
                try
                {
                    connection.Open();
                    MySqlDataAdapter command = new MySqlDataAdapter(cmdText, connection);
                    command.Fill(ds, startResord, maxRecord, "ds");
                }
                catch (MySql.Data.MySqlClient.MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }
                return ds.Tables[0];
            }
        }

        /// <summary> 
        /// 获取分页数据 在不用存储过程情况下 
        /// </summary> 
        /// <param name="recordCount">总记录条数</param> 
        /// <param name="selectList">选择的列逗号隔开,支持top num</param> 
        /// <param name="tableName">表名字</param> 
        /// <param name="whereStr">条件字符 必须前加 and</param> 
        /// <param name="orderExpression">排序 例如 ID</param> 
        /// <param name="pageIdex">当前索引页</param> 
        /// <param name="pageSize">每页记录数</param> 
        /// <returns></returns> 
        public DataTable getPager(out int recordCount, string selectList, string tableName, string whereStr, string orderExpression, int pageIdex, int pageSize)
        {
            int rows = 0;
            DataTable dt = new DataTable();
            MatchCollection matchs = Regex.Matches(selectList, @"top\s+\d{1,}", RegexOptions.IgnoreCase);//含有top 
            string sqlStr = sqlStr = string.Format("select {0} from {1} where 1=1 {2}", selectList, tableName, whereStr);
            if (!string.IsNullOrEmpty(orderExpression)) { sqlStr += string.Format(" Order by {0}", orderExpression); }
            if (matchs.Count > 0) //含有top的时候 
            {
                DataTable dtTemp = ExecuteDataTable(sqlStr);
                rows = dtTemp.Rows.Count;
            }
            else //不含有top的时候 
            {
                string sqlCount = string.Format("select count(*) from {0} where 1=1 {1} ", tableName, whereStr);
                //获取行数 
                object obj = ExecuteScalar(sqlCount);
                if (obj != null)
                {
                    rows = Convert.ToInt32(obj);
                }
            }
            dt = ExecuteDataTable(sqlStr, (pageIdex - 1) * pageSize, pageSize);
            recordCount = rows;
            return dt;
        }
    }
}
