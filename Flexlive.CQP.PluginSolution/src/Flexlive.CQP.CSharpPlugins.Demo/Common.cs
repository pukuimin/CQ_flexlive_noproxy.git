﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flexlive.CQP.Framework.Utils;

namespace Flexlive.CQP.CSharpPlugins.Demo
{
    public class Common
    {
        public static DateTime TimeStampToDateTime(int Time)
        {
            string timeStamp = Time.ToString();
            DateTime dtStart = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            long lTime = long.Parse(timeStamp + "0000000");
            TimeSpan toNow = new TimeSpan(lTime);
            DateTime dtResult = dtStart.Add(toNow);
            return dtResult;
        }
    }
    public static class Extension
    {
        public static DateTime ToDateTime(this int Time)
        {
            return Common.TimeStampToDateTime(Time);
        }
    }

    public class QQMessageHelper
    {
        #region ConnStr
        private static string _ConnStr = null;
        public static string ConnStr
        {
            get
            {
                if (_ConnStr == null)
                {
                    var filePath = FileHelper.GetAbsolutePath("/conf/QQMessageConn.txt");
                    if (!System.IO.File.Exists(filePath)) _ConnStr = "";
                    else _ConnStr = System.IO.File.ReadAllText(filePath, Encoding.UTF8);
                }
                return _ConnStr;
                //"Server=192.168.9.117;Port=3306;Database=ycf_stock;Uid=root;Pwd=ycf.com;Charset=utf8;Convert Zero Datetime=True";
            }
        }
        #endregion

        #region 写消息到mysql数据库
        /// <summary>
        /// 写消息到mysql数据库
        /// </summary>
        /// <param name="model">The model.</param>
        public static void InsertQQMessage(QQMessage model)
        {
            if (string.IsNullOrWhiteSpace(ConnStr)) return;//mysql连接串为空，不写数据库
            if (model == null) return;
            if (model.Content != null) model.Content = model.Content.Replace("'", "''");

            MySqlHelper mysql = new MySqlHelper(ConnStr);
            var sql = string.Format(@"
INSERT INTO `qqmessageinfo` (
    `SubType`,
    `SubTypeName`,
    `SendTime`,
    `FromQQ`,
    `FromDiscuss`,
    `FromGroup`,
    `FromAnonymous`,
    `Content`,
    `Remark`
) 
VALUES
    (
        {0},
        '{1}',
        '{2:yyyy-MM-dd HH:mm:ss}',
        '{3}',
        '{4}',
        '{5}',
        '{6}',
        '{7}',
        '{8}'
    );
", model.SubType, model.SubTypeName, model.SendTime, model.FromQQ, model.FromDiscuss, model.FromGroup, model.FromAnonymous, model.Content, model.Remark);
            mysql.ExecuteNonQuery(sql);
        }
        #endregion

        #region 通过网络接口获取QQ昵称
        /// <summary>
        /// 通过网络接口获取QQ昵称
        /// </summary>
        /// <param name="QQNumber">The qq number.</param>
        /// <returns></returns>
        public static string GetQQNick(string QQNumber)
        {
            if (string.IsNullOrEmpty(QQNumber)) return "";
            var url = "http://r.qzone.qq.com/fcg-bin/cgi_get_score.fcg?mask=7&uins=" + QQNumber + "&d=" + DateTime.Now.Ticks;
            var resStr = HttpHelper.Get(url);
            if (string.IsNullOrEmpty(resStr)) return "";
            var index1 = resStr.IndexOf('[');
            var index2 = resStr.IndexOf(']');
            var str = resStr.Substring(index1 + 1, index2 - index1 - 1);
            var strs = str.Replace("\"", "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (strs != null && strs.Length >= 7) return strs[6];
            return string.Empty;
        }
        #endregion
    }
    public class QQMessage
    {
        /// <summary>
        /// 自增ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 消息子类型
        /// </summary>
        public int SubType { get; set; }

        /// <summary>
        /// 消息子类型名称
        /// </summary>
        public string SubTypeName { get; set; }

        /// <summary>
        /// 消息时间
        /// </summary>
        public DateTime SendTime { get; set; }

        /// <summary>
        /// 来源QQ
        /// </summary>
        public string FromQQ { get; set; }

        /// <summary>
        /// 来源讨论组
        /// </summary>
        public string FromDiscuss { get; set; }

        /// <summary>
        /// 来源QQ群
        /// </summary>
        public string FromGroup { get; set; }

        /// <summary>
        /// 匿名信息
        /// </summary>
        public string FromAnonymous { get; set; }

        /// <summary>
        /// 消息内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}
