﻿using Flexlive.CQP.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Flexlive.CQP.CSharpPlugins.Demo
{
    /// <summary>
    /// 消息自动回复处理类
    /// </summary>
    public class MessageReplyHelper
    {
        public static readonly object lockObj = new object();
        /// <summary>
        /// 私聊消息回复处理
        /// </summary>
        /// <param name="msg">The MSG.</param>
        /// <returns></returns>
        public static string PrivateMessageReply(QQMessage message, long fromQQ)
        {
            var replyMsg = string.Empty;
            if (message == null || message.Content == null) return replyMsg;
            replyMsg = GetReplyByReceivedMsg(message.Content, fromQQ);

            if (!string.IsNullOrEmpty(replyMsg))
            {
                lock (lockObj)
                {
                    FileHelper.AppendTxt("自动回复消息：" + replyMsg, MyPlugin.TxtFilePath);
                }
            }

            return replyMsg;
        }



        /// <summary>
        /// 消息处理统计入口
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private static string GetReplyByReceivedMsg(string msg, long fromQQ)
        {
            var replyMsg = string.Empty;
            if (msg == null) return replyMsg;
            var msgReceives = msg.Trim().ToLowerInvariant();
            switch (msgReceives)
            {
                case "你好":
                case "您好":
                    replyMsg = String.Format("{0},我是小Q机器人~", msgReceives);
                    break;
                case "现在几点":
                case "几点了":
                    replyMsg = String.Format("您好,现在时间：{0:HH:mm:ss}", DateTime.Now);
                    break;
                case "今天周几":
                case "今天星期几":
                    replyMsg = String.Format("您好,今天是星期{0}", GetWeekNo(DateTime.Now.DayOfWeek));
                    break;
                case "testcq":
                    replyMsg = String.Format("插件正常工作中，消息内容：{0} {1}", msgReceives, CQ.CQCode_Face(14));
                    break;
                case "谢谢":
                case "谢谢！":
                case "谢谢!":
                case "谢谢。":
                case "谢谢.":
                    replyMsg = String.Format("不客气 {0}", CQ.CQCode_Face(21));
                    break;
                //case ".net开发牛逼吗?":
                //    replyMsg = "当然牛逼，是吹牛B！";
                //    break;
                //case "本次分享内容":
                //    replyMsg = "http://www.pukuimin.top/post/239.html";
                //break;
                //case "imagetest"://"<img src=\"http://cdn01.pukuimin.top/zb_users/upload/netcodemaker.jpg\" width=200 style=\"border: 12px #987cb9 solid\">";
                //replyMsg = CQ.CQCode_Image("netcodemaker.jpg");
                //break;
                case "你很傻":
                    {
                        if(fromQQ == 772374558)
                        {
                            replyMsg = "主人说我傻我只好承认，不然会被打的！";
                        }
                    }
                    break;
                default:
                    {
                        if (msgReceives.Contains("[CQ:face,id=46]"))
                        {
                            replyMsg = CQ.CQCode_Face(46);
                        }
                    }
                    break;
            }
            if (string.IsNullOrEmpty(replyMsg))
            {
                replyMsg = TulingHelper.RequestTulingAndGetResponseByMsg(msgReceives, fromQQ);
                if (!string.IsNullOrEmpty(replyMsg))
                {
                    replyMsg.TrimEnd('\n').TrimEnd('\r');
                }
            }
            return replyMsg;
        }

        private static string atPattern = "\\[CQ:at,qq=(\\d+)\\]";

        /// <summary>
        /// 群聊或讨论组消息回复处理
        /// </summary>
        /// <param name="msg">The MSG.</param>
        /// <returns></returns>
        public static string GroupOrDiscussMessageReply(QQMessage qqMessage, long fromQQ)
        {
            if (qqMessage == null || qqMessage.Content == null) return string.Empty;
            var atCode = atMeMessage;
            if (qqMessage.Content.Contains(atCode))
            {
                var replyMsg = GetReplyByReceivedMsg(Regex.Replace(qqMessage.Content, atPattern, ""), fromQQ);
                if (string.IsNullOrEmpty(replyMsg))
                {
                    return String.Format("{0} 消息已收到，指令无法识别~", CQ.CQCode_At(Convert.ToInt64(qqMessage.FromQQ)));
                }
                else
                {
                    return String.Format("{0} {1}", CQ.CQCode_At(Convert.ToInt64(qqMessage.FromQQ)), replyMsg);
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// 获取周几
        /// </summary>
        /// <param name="dow">The dow.</param>
        /// <returns></returns>
        private static string GetWeekNo(DayOfWeek dow)
        {
            switch (dow)
            {
                case DayOfWeek.Monday: return "一";
                case DayOfWeek.Tuesday: return "二";
                case DayOfWeek.Wednesday: return "三";
                case DayOfWeek.Thursday: return "四";
                case DayOfWeek.Friday: return "五";
                case DayOfWeek.Saturday: return "六";
                case DayOfWeek.Sunday: return "天";
                default: return "";
            }
        }

        private static string _atMeMessage = "";
        public static string atMeMessage
        {
            get
            {
                if (string.IsNullOrEmpty(_atMeMessage))
                {
                    _atMeMessage = "[CQ:at,qq=" + CQ.GetLoginQQ() + "]";
                }
                return _atMeMessage;
            }
        }

    }

}
