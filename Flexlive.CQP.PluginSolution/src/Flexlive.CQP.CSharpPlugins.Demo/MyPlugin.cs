﻿using Flexlive.CQP.Framework;
using System;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace Flexlive.CQP.CSharpPlugins.Demo
{
    /// <summary>
    /// 酷Q C#版插件Demo
    /// </summary>
    public class MyPlugin : CQAppAbstract
    {
        public static string TxtFilePath
        {
            get
            {
                if (string.IsNullOrEmpty(_TxtFilePath)) _TxtFilePath = string.Format("/data/log/QQMessageList{0}.txt", DateTime.Today.ToString("yyyyMMdd"));
                return _TxtFilePath;
            }
        }
        private static string _TxtFilePath = "";
        public static readonly object lockObj = new object();


        public string GetSubTypeName(int subType)
        {
            var ret = "";
            switch (subType)
            {
                case 11:
                    ret = "好友";
                    break;
                case 1:
                    ret = "在线状态";
                    break;
                case 2:
                    ret = "群";
                    break;
                case 3:
                    ret = "来自讨论组";
                    break;
            }
            return ret;
        }

        public string DealMsg(string msg)
        {
            if (string.IsNullOrEmpty(msg)) return msg;
            //var msg = "[CQ:image,file=A7D637E6F29F199D7D2ADDEB2A1D21B4.png][CQ:image,file=A7D637E6F29F199D7D2ADDEB2A1D21B5.png]adad";
            Regex reg = new Regex(@"\[CQ:image,file=(?<filename>[^\s\r\n\]]+)\]", RegexOptions.IgnoreCase);
            MatchCollection matches = reg.Matches(msg);
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    var imagename = match.Groups["filename"].Value;
                    var imageinfofile = FileHelper.GetAbsolutePath("/data/image/" + imagename + ".cqimg");
                    //var lines = System.IO.File.ReadAllLines();
                    var imageUrl = fileHandle.INIGetStringValue(imageinfofile, "image", "url", "");
                    msg += "【" + imageUrl + "】";
                }
            }
            return msg;
        }

        /// <summary>
        /// 应用初始化，用来初始化应用的基本信息。
        /// </summary>
        public override void Initialize()
        {
            // 此方法用来初始化插件名称、版本、作者、描述等信息，
            // 不要在此添加其它初始化代码，插件初始化请写在Startup方法中。

            this.Name = "Flexlive官方示例插件";
            this.Version = new Version("1.0.0.0");
            this.Author = "Flexlive";
            this.Description = "基于Flexlive版 酷Q C#开方框架的酷Q插件示例。";
        }

        /// <summary>
        /// 应用启动，完成插件线程、全局变量等自身运行所必须的初始化工作。
        /// </summary>
        public override void Startup()
        {
            //完成插件线程、全局变量等自身运行所必须的初始化工作。

        }

        /// <summary>
        /// 打开设置窗口。
        /// </summary>
        public override void OpenSettingForm()
        {
            // 打开设置窗口的相关代码。
            FormSettings frm = new FormSettings();
            frm.ShowDialog();
        }

        #region private 私有方法

        private static long FixLong(long number)
        {
            if (number > 0)
                return number;

            unchecked
            {
                return (uint)(int)number;
            }
        }


        #endregion

        #region 私聊消息处理
        /// <summary>
        /// Type=21 私聊消息。
        /// </summary>
        /// <param name="subType">子类型，11/来自好友 1/来自在线状态 2/来自群 3/来自讨论组。</param>
        /// <param name="sendTime">发送时间(时间戳)。</param>
        /// <param name="fromQQ">来源QQ。</param>
        /// <param name="msg">消息内容。</param>
        /// <param name="font">字体。</param>
        public override void PrivateMessage(int subType, int sendTime, long fromQQ, string msg, int font)
        {
            fromQQ = FixLong(fromQQ);
            var qqMessage = new QQMessage
            {
                SubType = subType,
                SubTypeName = GetSubTypeName(subType),
                SendTime = sendTime.ToDateTime(),
                FromQQ = fromQQ.ToString(),
                Content = DealMsg(msg),
                Remark = "私聊消息",
            };
            if (qqMessage.SendTime < DateTime.Now.AddMinutes(-10)) qqMessage.SendTime = DateTime.Now;
            var msgContent = string.Format("消息类型：{0},时间：{1}({5}),来源QQ：{2}({4}),消息内容：{3}\r\n", qqMessage.SubTypeName, qqMessage.SendTime.ToString("yyyy-MM-dd HH:mm:ss"), qqMessage.FromQQ, qqMessage.Content, QQMessageHelper.GetQQNick(qqMessage.FromQQ), sendTime);

            lock (lockObj)
            {
                FileHelper.AppendTxt(msgContent, TxtFilePath);
            }
            try
            {
                QQMessageHelper.InsertQQMessage(qqMessage);
            }
            catch (Exception ex)
            {
                lock (lockObj)
                {
                    FileHelper.AppendTxt("InsertQQMessage出错:" + ex.Message, TxtFilePath);
                }
            }

            //消息自动回复示例
            var replyMsg = MessageReplyHelper.PrivateMessageReply(qqMessage, fromQQ);

            // 处理私聊消息。
            if (!string.IsNullOrEmpty(replyMsg)) CQ.SendPrivateMessage(fromQQ, replyMsg);
        }
        #endregion

        #region 群消息处理
        /// <summary>
        /// Type=2 群消息。
        /// </summary>
        /// <param name="subType">子类型，目前固定为1。</param>
        /// <param name="sendTime">发送时间(时间戳)。</param>
        /// <param name="fromGroup">来源群号。</param>
        /// <param name="fromQQ">来源QQ。</param>
        /// <param name="fromAnonymous">来源匿名者。</param>
        /// <param name="msg">消息内容。</param>
        /// <param name="font">字体。</param>
        public override void GroupMessage(int subType, int sendTime, long fromGroup, long fromQQ, string fromAnonymous, string msg, int font)
        {
            //CQGroupMemberInfo groupMember = null;
            //try
            //{
            //    groupMember = CQ.GetGroupMemberInfo(fromGroup, fromQQ);
            //}
            //catch
            //{
            //    groupMember = new CQGroupMemberInfo();
            //}
            QQMessage qqMessage = null;
            try
            {
                fromQQ = FixLong(fromQQ);
                fromGroup = FixLong(fromGroup);
                qqMessage = new QQMessage
                {
                    SubType = subType,
                    SubTypeName = GetSubTypeName(subType),
                    SendTime = sendTime.ToDateTime(),
                    FromQQ = fromQQ.ToString(),
                    FromGroup = fromGroup.ToString(),
                    Content = DealMsg(msg),
                    FromAnonymous = fromAnonymous,
                    Remark = "QQ群消息",
                };
                if (qqMessage.SendTime < DateTime.Now.AddMinutes(-10)) qqMessage.SendTime = DateTime.Now;
                var msgContent = string.Format("消息类型：{0},时间：{1}({7}),来源QQ群：{2},来源QQ：{3}({6}),匿名：{4},消息内容：{5}\r\n", qqMessage.SubTypeName, qqMessage.SendTime.ToString("yyyy-MM-dd HH:mm:ss"), fromGroup, fromQQ, fromAnonymous, qqMessage.Content, QQMessageHelper.GetQQNick(qqMessage.FromQQ), sendTime);

                lock (lockObj)
                {
                    FileHelper.AppendTxt(msgContent, TxtFilePath);
                }
                try
                {
                    QQMessageHelper.InsertQQMessage(qqMessage);
                }
                catch (Exception ex)
                {
                    lock (lockObj)
                    {
                        FileHelper.AppendTxt("InsertQQMessage出错:" + ex.Message, TxtFilePath);
                    }
                }
            }
            catch (Exception ex)
            {
                lock (lockObj)
                {
                    FileHelper.AppendTxt(ex.GetType() + ":" + ex.Message, TxtFilePath);
                }

            }
            try
            {//当有人@我时，回复“收到”
                if (qqMessage.Content != null)
                {
                    var replyMsg = MessageReplyHelper.GroupOrDiscussMessageReply(qqMessage, fromQQ);
                    if (!string.IsNullOrEmpty(replyMsg))
                    {
                        CQ.SendGroupMessage(fromGroup, replyMsg);
                    }
                }
            }
            catch (Exception ex)
            {
                lock (lockObj)
                {
                    FileHelper.AppendTxt(ex.GetType() + ":" + ex.Message, TxtFilePath);
                }
            }
            //try
            //{
            //    Stopwatch sw = new Stopwatch();
            //    sw.Start();
            //    var groupMember = CQ.GetGroupMemberInfo(fromGroup, fromQQ);
            //    var groupInfo = string.Format(" QQ号:{0},群号:{1},GroupCard:{2},QQName:{3}\r\n", groupMember.QQNumber,groupMember.GroupNumber,groupMember.GroupCard,groupMember.QQName);
            //    sw.Stop();
            //    groupInfo = sw.ElapsedMilliseconds + groupInfo;
            //    lock (lockObj)
            //    {
            //        FileHelper.AppendTxt(groupInfo, TxtFilePath);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    lock (lockObj)
            //    {
            //        FileHelper.AppendTxt("GetGroupMemberInfo出错:" + ex.Message, TxtFilePath);
            //    }

            //}
            //// 处理群消息。
            //var groupMember = CQ.GetGroupMemberInfo(fromGroup, fromQQ);

            //CQ.SendGroupMessage(fromGroup, String.Format("[{4}]{0} 你的群名片：{1}， 入群时间：{2}， 最后发言：{3}。", CQ.CQCode_At(fromQQ),
            //    groupMember.GroupCard, groupMember.JoinTime, groupMember.LastSpeakingTime, CQ.ProxyType));
            //CQ.SendGroupMessage(fromGroup, String.Format("[{0}]{1}你发的群消息是：{2}", CQ.ProxyType, CQ.CQCode_At(fromQQ), msg));
        }
        #endregion

        #region 讨论组消息处理
        /// <summary>
        /// Type=4 讨论组消息。
        /// </summary>
        /// <param name="subType">子类型，目前固定为1。</param>
        /// <param name="sendTime">发送时间(时间戳)。</param>
        /// <param name="fromDiscuss">来源讨论组。</param>
        /// <param name="fromQQ">来源QQ。</param>
        /// <param name="msg">消息内容。</param>
        /// <param name="font">字体。</param>
        public override void DiscussMessage(int subType, int sendTime, long fromDiscuss, long fromQQ, string msg, int font)
        {
            fromQQ = FixLong(fromQQ);
            fromDiscuss = FixLong(fromDiscuss);
            var qqMessage = new QQMessage
            {
                SubType = subType,
                SubTypeName = GetSubTypeName(subType),
                SendTime = sendTime.ToDateTime(),
                FromQQ = fromQQ.ToString(),
                FromDiscuss = fromDiscuss.ToString(),
                Content = DealMsg(msg),
                Remark = "讨论组消息",
            };
            if (qqMessage.SendTime < DateTime.Now.AddMinutes(-10)) qqMessage.SendTime = DateTime.Now;
            var msgContent = string.Format("消息类型：{0},时间：{1}({6}),来源讨论组：{2},来源QQ：{3}({5}),消息内容：{4}\r\n", qqMessage.SubTypeName, qqMessage.SendTime.ToString("yyyy-MM-dd HH:mm:ss"), fromDiscuss, fromQQ, qqMessage.Content, QQMessageHelper.GetQQNick(qqMessage.FromQQ), sendTime);
            lock (lockObj)
            {
                FileHelper.AppendTxt(msgContent, TxtFilePath);
            }
            try
            {
                QQMessageHelper.InsertQQMessage(qqMessage);
            }
            catch (Exception ex)
            {
                lock (lockObj)
                {
                    FileHelper.AppendTxt("InsertQQMessage出错:" + ex.Message, TxtFilePath);
                }
            }

            try
            {//当有人@我时，回复“收到”
                if (qqMessage.Content != null)
                {
                    var replyMsg = MessageReplyHelper.GroupOrDiscussMessageReply(qqMessage, fromQQ);
                    if (!string.IsNullOrEmpty(replyMsg))
                    {
                        CQ.SendDiscussMessage(fromDiscuss, replyMsg);
                    }
                }
            }
            catch (Exception ex)
            {
                lock (lockObj)
                {
                    FileHelper.AppendTxt(ex.GetType() + ":" + ex.Message, TxtFilePath);
                }
            }
            //// 处理讨论组消息。
            //CQ.SendDiscussMessage(fromDiscuss, String.Format("[{0}]{1}你发的讨论组消息是：{2}", CQ.ProxyType, CQ.CQCode_At(fromQQ), msg));
        }
        #endregion

        /// <summary>
        /// Type=11 群文件上传事件。
        /// </summary>
        /// <param name="subType">子类型，目前固定为1。</param>
        /// <param name="sendTime">发送时间(时间戳)。</param>
        /// <param name="fromGroup">来源群号。</param>
        /// <param name="fromQQ">来源QQ。</param>
        /// <param name="file">上传文件信息。</param>
        public override void GroupUpload(int subType, int sendTime, long fromGroup, long fromQQ, string file)
        {
            // 处理群文件上传事件。
            CQ.SendGroupMessage(fromGroup, String.Format("[{0}]{1}你上传了一个文件：{2}", CQ.ProxyType, CQ.CQCode_At(fromQQ), file));
        }

        /// <summary>
        /// Type=101 群事件-管理员变动。
        /// </summary>
        /// <param name="subType">子类型，1/被取消管理员 2/被设置管理员。</param>
        /// <param name="sendTime">发送时间(时间戳)。</param>
        /// <param name="fromGroup">来源群号。</param>
        /// <param name="beingOperateQQ">被操作QQ。</param>
        public override void GroupAdmin(int subType, int sendTime, long fromGroup, long beingOperateQQ)
        {
            //// 处理群事件-管理员变动。
            //CQ.SendGroupMessage(fromGroup, String.Format("[{0}]{2}({1})被{3}管理员权限。", CQ.ProxyType, beingOperateQQ, CQE.GetQQName(beingOperateQQ), subType == 1 ? "取消了" : "设置为"));
        }

        /// <summary>
        /// Type=102 群事件-群成员减少。
        /// </summary>
        /// <param name="subType">子类型，1/群员离开 2/群员被踢 3/自己(即登录号)被踢。</param>
        /// <param name="sendTime">发送时间(时间戳)。</param>
        /// <param name="fromGroup">来源群。</param>
        /// <param name="fromQQ">来源QQ。</param>
        /// <param name="beingOperateQQ">被操作QQ。</param>
        public override void GroupMemberDecrease(int subType, int sendTime, long fromGroup, long fromQQ, long beingOperateQQ)
        {
            //// 处理群事件-群成员减少。
            //CQ.SendGroupMessage(fromGroup, String.Format("[{0}]群员{2}({1}){3}", CQ.ProxyType, beingOperateQQ, CQE.GetQQName(beingOperateQQ), subType == 1 ? "退群。" : String.Format("被{0}({1})踢除。", CQE.GetQQName(fromQQ), fromQQ)));
        }

        /// <summary>
        /// Type=103 群事件-群成员增加。
        /// </summary>
        /// <param name="subType">子类型，1/管理员已同意 2/管理员邀请。</param>
        /// <param name="sendTime">发送时间(时间戳)。</param>
        /// <param name="fromGroup">来源群。</param>
        /// <param name="fromQQ">来源QQ。</param>
        /// <param name="beingOperateQQ">被操作QQ。</param>
        public override void GroupMemberIncrease(int subType, int sendTime, long fromGroup, long fromQQ, long beingOperateQQ)
        {
            fromGroup = FixLong(fromGroup);
            fromQQ = FixLong(fromQQ);
            beingOperateQQ = FixLong(beingOperateQQ);
            //// 处理群事件-群成员增加。
            //CQ.SendGroupMessage(fromGroup, String.Format("[{0}]群里来了新人{2}({1})，管理员{3}({4}){5}", CQ.ProxyType, beingOperateQQ, CQE.GetQQName(beingOperateQQ), CQE.GetQQName(fromQQ), fromQQ, subType == 1 ? "同意。" : "邀请。"));
            switch (fromGroup)
            {
                case 703666461:
                    {
                        CQ.SendGroupMessage(fromGroup, $"{CQ.CQCode_At(beingOperateQQ)}({beingOperateQQ})，欢迎加入要出发开发交流群！\r\n入群方式：由管理员{CQE.GetQQName(fromQQ)}({fromQQ}){(subType == 1 ? "同意。" : "邀请。")}\r\n新入群同事请修改群名片，格式如：java-XX/.net-XX/php-XX/前端-XX/运营-XX，谢谢！\r\n加入此群请大家合理发言，谢谢配合！");
                    }
                    break;
            }
        }

        /// <summary>
        /// Type=201 好友事件-好友已添加。
        /// </summary>
        /// <param name="subType">子类型，目前固定为1。</param>
        /// <param name="sendTime">发送时间(时间戳)。</param>
        /// <param name="fromQQ">来源QQ。</param>
        public override void FriendAdded(int subType, int sendTime, long fromQQ)
        {
            //// 处理好友事件-好友已添加。
            //CQ.SendPrivateMessage(fromQQ, String.Format("[{0}]你好，我的朋友！", CQ.ProxyType));
        }

        /// <summary>
        /// Type=301 请求-好友添加。
        /// </summary>
        /// <param name="subType">子类型，目前固定为1。</param>
        /// <param name="sendTime">发送时间(时间戳)。</param>
        /// <param name="fromQQ">来源QQ。</param>
        /// <param name="msg">附言。</param>
        /// <param name="responseFlag">反馈标识(处理请求用)。</param>
        public override void RequestAddFriend(int subType, int sendTime, long fromQQ, string msg, string responseFlag)
        {
            //// 处理请求-好友添加。
            //CQ.SetFriendAddRequest(responseFlag, CQReactType.Allow, "新来的朋友");
        }

        /// <summary>
        /// Type=302 请求-群添加。
        /// </summary>
        /// <param name="subType">子类型，目前固定为1。</param>
        /// <param name="sendTime">发送时间(时间戳)。</param>
        /// <param name="fromGroup">来源群号。</param>
        /// <param name="fromQQ">来源QQ。</param>
        /// <param name="msg">附言。</param>
        /// <param name="responseFlag">反馈标识(处理请求用)。</param>
        public override void RequestAddGroup(int subType, int sendTime, long fromGroup, long fromQQ, string msg, string responseFlag)
        {
            //// 处理请求-群添加。
            //CQ.SetGroupAddRequest(responseFlag, CQRequestType.GroupAdd, CQReactType.Allow, "新群友");
        }
    }
}
