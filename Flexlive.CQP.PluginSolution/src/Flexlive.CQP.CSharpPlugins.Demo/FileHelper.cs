﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Flexlive.CQP.CSharpPlugins.Demo
{
    public class FileHelper
    {

        public static void AppendTxt(string text, string relationPath = "")
        {
            try
            {
                if (relationPath == "") relationPath = string.Format("/Upload/mylog{0:yyyyMMdd}.txt", DateTime.Now);
                string txtPath = "";//获取绝对路径
                txtPath = GetAbsolutePath(relationPath);

                using (FileStream fs = new FileStream(txtPath, FileMode.OpenOrCreate | FileMode.Append, FileAccess.Write, FileShare.Read))
                {
                    using (StreamWriter sw = new StreamWriter(fs, Encoding.Default))
                    {
                        sw.Write(text);
                        sw.Flush();
                        sw.Close();
                    }
                }
                return;
            }
            catch { throw new Exception("文件夹没有写入权限！"); }
        }

        /// <summary>
        /// 通过相对路径获取文件夹绝对路径
        /// </summary>
        /// <param name="relativePath"></param>
        /// <returns></returns>
        public static string GetAbsolutePath(string relativePath)
        {
            string AbsolutePath = "";
            relativePath = relativePath.Replace("/", "\\");
            if (relativePath.StartsWith("\\"))//确定 String 实例的开头是否与指定的字符串匹配。为下边的合并字符串做准备
            {
                relativePath = relativePath.TrimStart('\\');//从此实例的开始位置移除数组中指定的一组字符的所有匹配项。为下边的合并字符串做准备
            }
            AbsolutePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, relativePath);
            return AbsolutePath;
        }
    }

}
